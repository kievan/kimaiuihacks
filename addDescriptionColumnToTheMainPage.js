/**
 *
 * Add a 'Description' column to the main page 
 * and populate it with descriptions of the corresponding timesheet entries.
 * 
 * Limitations: Once you trigger an action that requires page reload 
 * (e.g. Re-record) the 'Description' column disappears.
 *  
 */

(function() {
    'use strict';

    // Change column name
	document.querySelectorAll("tbody>tr>td[class=username]")[0].innerHTML = 'Description';

    var timeSheetEntry_id_list = [];
    function updateTimeSheetEntryIdList(id, description){
	    timeSheetEntry_id_list.push({id:id,description:description});
    }
	
	// Get time sheet entry id list
    var timeSheetEntry_id_list_raw = document.querySelectorAll("tr[id*=timeSheetEntry]");
    for(var i=0; i < timeSheetEntry_id_list_raw.length; i++){
    	var timeSheetEntry_id = document.querySelectorAll("tr[id*=timeSheetEntry]")[i].id.replace('timeSheetEntry','');
    	updateTimeSheetEntryIdList(timeSheetEntry_id, 'Dummy for ' + id);
    }

    console.log(timeSheetEntry_id_list);

	var newIdList = [];
	function updateNewIdList(id, description){
	    newIdList.push({id:id,description:description});
    }
    // Get timeSheetEntry descriptions and populate them
   	var j = 0;
   	var id = timeSheetEntry_id_list[j].id;
   	console.log('Get Desc Ajax - time sheet entry id: ', id);
   	populateDescriptionColumn(id);

    function populateDescriptionColumn(id){
	    $.ajax(
	        {
	            method:"POST",
	            url:"http://timetracking.sixt.de/kimai/extensions/ki_timesheets/floaters.php?axAction=add_edit_timeSheetEntry&axValue=0&id="+id
	        }
	    ).done(
	        function(html) {
	            html = '<html>'+html+'</html>';
	            var description = $("#description", html).html();
				document.querySelectorAll("tr[id=timeSheetEntry"+id+"]>td")[8].innerHTML = description;
				console.log('Populating description({id:id,description:description}):', {id:id,description:description});
				console.log("J: ", j);
				if(j < timeSheetEntry_id_list.length - 1){
					j++;
					console.log("J++: ", j);
					populateDescriptionColumn(timeSheetEntry_id_list[j].id);
				} else {
					console.log('Done populating description column.');
				}
	        }
	    );
    }

})();
